// Konsolenausgabe (�bung2)



public class Aufgabe1 {


	public static void main(String[] args) {
		
		String s ="**";
		String d ="*";
		System.out.printf( "%5s\n", s );
		System.out.printf( "%s", d );
		System.out.printf( "%7s\n", d );
		
		System.out.printf( "%s", d );
		System.out.printf( "%7s\n", d );
		System.out.printf( "%5s\n", s );
		System.out.printf(" \n");
		
//Aufgabe 2 		
		
		System.out.printf( "%s", "0!"); 
		System.out.printf( "%6s", "=" );
		System.out.printf( "%s", "" ); // Hier eintragen
		System.out.printf("%20s","=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf( "%s", "1!"); 
		System.out.printf( "%6s", "=" );
		System.out.printf( "%s", " 1" ); // Hier eintragen
		System.out.printf("%18s","=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf( "%s", "2!"); 
		System.out.printf( "%6s", "=" );
		System.out.printf( "%s", " 1 * 2" ); // Hier eintragen
		System.out.printf("%14s","=");
		System.out.printf("%4s\n", "2");
		
		System.out.printf( "%s", "3!"); 
		System.out.printf( "%6s", "=" );
		System.out.printf( "%s", " 1 * 2 * 3" ); // Hier eintragen
		System.out.printf("%10s","=");
		System.out.printf("%4s\n", "6");
		
		System.out.printf( "%s", "4!"); 
		System.out.printf( "%6s", "=" );
		System.out.printf( "%s", " 1 * 2 * 3 * 4" ); // Hier eintragen
		System.out.printf("%6s","=");
		System.out.printf("%4s\n", "24");
		
		System.out.printf( "%s", "5!"); 
		System.out.printf( "%6s", "=" );
		System.out.printf( "%s", " 1 * 2 * 3 * 4 * 5" ); // Hier eintragen
		System.out.printf("%2s","=");
		System.out.printf("%4s\n", "120");
		System.out.printf("\n");
		
		
		
//Aufgabe 3 Temperaturen
	
		System.out.printf("%s %4s %10s \n", "Fahrenheit", "|" , "Celsius");
		System.out.printf("%s\n", "---------------------------");
		System.out.printf("%s%12s %10.6s \n", -20, "|" , -28.8889);
		System.out.printf("%s%12s %10.6s \n", -10, "|" , -23.3333);		
		System.out.printf(" %s%13s %10.6s \n", +0, "|" , -17.7778);
		System.out.printf(" %s%12s %10.6s \n", +20, "|" , -6.6667);
		System.out.printf(" %s%12s %10.6s \n", +30, "|" , -1.1111);
	
	}
}