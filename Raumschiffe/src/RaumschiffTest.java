
public class RaumschiffTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Ladung lad1 = new Ladung("Ferengi Schneckensaft",200);
		Ladung lad2 = new Ladung("Borg-Schrott",5);
		Ladung lad3 = new Ladung("Rore Materie",2);
		Ladung lad4 = new Ladung("Forschungssonde",35);
		Ladung lad5 = new Ladung("Bat´leth Klington Schwert",200);
		Ladung lad6 = new Ladung("Plasma-Waffe",50);
		Ladung lad7 = new Ladung("Photonentorpedo",3);
		
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,2, "IKS Hegh´ta");
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,2, "IKR Khazara");
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,5, "Ni'Var");

		klingonen.addLadung(lad1);
		klingonen.addLadung(lad5);
		
		romulaner.addLadung(lad2);
		romulaner.addLadung(lad3);
		romulaner.addLadung(lad6);
		
		vulkanier.addLadung(lad4);
		vulkanier.addLadung(lad7);

		//klingonen.zustandRaumschiff();
		
		//klingonen.ladungsverzeichnisAusgeben();
		
		
		//romulaner.nachrichtAnAlle("Affen");
		//System.out.print(Raumschiff.broadcastKommunikator);
		
		//System.out.println(lad1);
		
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		romulaner.zustandRaumschiff();
		vulkanier.zustandRaumschiff();
		
		System.out.println(Raumschiff.broadcastKommunikator);
		
		
		

}
}