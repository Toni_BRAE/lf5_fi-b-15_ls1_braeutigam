import java.util.ArrayList;
import java.util.List;

public class Raumschiff {
 private int photonentorpedoAnzahl;
 private int energieversorgungInProzent;
 private int schildeInProzent;
 private int huelleInProzent;
 private int lebenserhaltungssystemeInProzent;
 private int androidenAnzahl;
 private String schiffsname;
 public static ArrayList<String> broadcastKommunikator; //public für Test
 private ArrayList<Ladung> ladungsverzeichnis;
  
public Raumschiff() {
	if(this.broadcastKommunikator == null) {
		this.broadcastKommunikator = new ArrayList<>();
	}
} 
  
public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzet, int schildeInProzent, int huelleInProzent,
		int ebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
	super();
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	this.energieversorgungInProzent = energieversorgungInProzet;
	this.schildeInProzent = schildeInProzent;
	this.huelleInProzent = huelleInProzent;
	this.lebenserhaltungssystemeInProzent = ebenserhaltungssystemeInProzent;
	this.androidenAnzahl = androidenAnzahl;
	this.schiffsname = schiffsname;
	this.ladungsverzeichnis = new ArrayList<>();
	if(this.broadcastKommunikator == null) {
		this.broadcastKommunikator = new ArrayList<>();
	}
}

public int getPhotonentorpedoAnzahl() {
	return photonentorpedoAnzahl;
}


public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
}


public int getEnergieversorgungInProzent() {
	return energieversorgungInProzent;
}


public void setEnergieversorgungInProzent(int energieversorgungInProzet) {
	this.energieversorgungInProzent = energieversorgungInProzet;
}


public int getSchildeInProzent() {
	return schildeInProzent;
}


public void setSchildeInProzent(int schildeInProzent) {
	this.schildeInProzent = schildeInProzent;
}


public int getHuelleInProzent() {
	return huelleInProzent;
}


public void setHuelleInProzent(int huelleInProzent) {
	this.huelleInProzent = huelleInProzent;
}


public int getLebenserhaltungssystemeInProzent() {
	return lebenserhaltungssystemeInProzent;
}


public void setLebenserhaltungssystemeInProzent(int ebenserhaltungssystemeInProzent) {
	this.lebenserhaltungssystemeInProzent = ebenserhaltungssystemeInProzent;
}


public int getAndroidenAnzahl() {
	return androidenAnzahl;
}


public void setAndroidenAnzahl(int androidenAnzahl) {
	this.androidenAnzahl = androidenAnzahl;
}


public String getSchiffsname() {
	return schiffsname;
}


public void setSchiffsname(String schiffsname) {
	this.schiffsname = schiffsname;
}

public void addLadung(Ladung neueLadung) {
	ladungsverzeichnis.add(neueLadung);
}

public void photonentorpedoSchiessen(Raumschiff r) {
	if (photonentorpedoAnzahl == 0) {
		broadcastKommunikator.add("-=*Click*=-");
	
	}else{
		--photonentorpedoAnzahl;
		broadcastKommunikator.add("Photonentorpedo abgeschossen");
		treffer(r);	
	}	
	
}

public void phaserkanoneSchiessen(Raumschiff r) {
	if (energieversorgungInProzent < 50) {
		broadcastKommunikator.add("-=*Click*=-");
	}else {
		energieversorgungInProzent= energieversorgungInProzent /2;
		broadcastKommunikator.add("Phaserkanone abgeschossen");	
		treffer(r);
	}
}

private void treffer(Raumschiff r) {
	r.setSchildeInProzent(r.getSchildeInProzent()/2);
	if (r.getSchildeInProzent()==0) {
		r.setHuelleInProzent(r.getHuelleInProzent()/2);
		r.setEnergieversorgungInProzent(getEnergieversorgungInProzent()/2);
	if (r.getHuelleInProzent()==0) {	
		r.setLebenserhaltungssystemeInProzent(0);
		broadcastKommunikator.add("Lebenserhaltungssysteme wurden vernichtet!");	
	}
	}
}

public void nachrichtAnAlle(String message) {
	broadcastKommunikator.add(message);
}

private static ArrayList <String> eintraegeLogbuchZurueckgeben() {
	return broadcastKommunikator;
}

public void photonentorpedosLaden(int anzahlTorpedos) {
}

public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
		boolean schiffhuelle, boolean anzahlDroiden) {	
}

public void zustandRaumschiff() {
	System.out.println("Raumschiff: " + schiffsname);
	System.out.println("Schildezustand: " + schildeInProzent);
	System.out.println("Huellezustand: " + huelleInProzent);
	System.out.println("Lebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent);
	
}	
	
public void ladungsverzeichnisAusgeben() {
	for (int i = 0; i < ladungsverzeichnis.size(); i++ ) {
		Ladung ladung = ladungsverzeichnis.get(i);
		System.out.println("Bezeichnung: "+ladung.getBezeichnung());
		System.out.println("Menge: "+ladung.getMenge());
		System.out.println("");
		

}	
}
}
 

 