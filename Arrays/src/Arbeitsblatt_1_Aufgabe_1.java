
public class Arbeitsblatt_1_Aufgabe_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// int-Array mit 10 Elementen erstellen:
		int[] zahlen = new int[10];
		
		// Zahlen 0 bis 9 eintragen:
		for(int i = 0; i < 10; i++) {
			zahlen[i] = i;
		}
		
		// Array "zahlen" auf der Konsole ausgeben:
		for(int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
	}
}


