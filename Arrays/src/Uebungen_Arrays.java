
public class Uebungen_Arrays {

	public static void main(String[] args) {
		
		double[] temperatur = new double[10];
		temperatur[0]= 4.0;
		temperatur[1]= 5.0;
		temperatur[2]= 6.0;
		temperatur[3]= 7.0;
		temperatur[4]= 8.0;
		temperatur[5]= 9.0;
		temperatur[6]= 10.0;
		temperatur[7]= 11.0;
		temperatur[8]= 12.0;
		temperatur[9]= 13.0;
		
		temperatur=new double[] {14.0,15.0,16.0,17.0};
		System.out.println(temperatur.length); //nachfragen warum hier nur die 4 ausgegeben wird

		//for (int index = 0; index < temperatur.length; index++) {
				//System.out.println(temperatur[index]);
	
		double temperaturMittelwert = 0.0;
		for (int index = 0; index < temperatur.length; index++) {
			temperaturMittelwert += temperatur[index];
		}
		temperaturMittelwert /= temperatur.length;
		System.out.println(temperaturMittelwert);
		
		
}
}