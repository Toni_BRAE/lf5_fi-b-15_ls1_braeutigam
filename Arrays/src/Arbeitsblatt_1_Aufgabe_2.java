
public class Arbeitsblatt_1_Aufgabe_2 {
	public static void main(String[] args) {
		

			// int-Array mit 10 Elementen erstellen:
			int[] zahlen = new int[10];
			
			// Ungerade Zahlen 1 bis 19 eintragen:
			for(int i = 0; i < zahlen.length; i++) {
				zahlen[i] = 2 * i + 1;
			}
			
			// Array "zahlen" auf der Konsole ausgeben:
			for(int i = 0; i < zahlen.length; i++) {
				System.out.println(zahlen[i]);
			}
		}
	}
