import java.sql.SQLNonTransientConnectionException;
import java.util.Scanner;
import java.util.concurrent.Delayed;
import java.util.function.Function;

import org.omg.CORBA.PUBLIC_MEMBER;

class Fahrkartenautomat_methoden {
	public static void main(String[] args) {

		double ticketPreis;
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		double rueckgabebetrag;
		int millisekunde;

		while (true) {
			zuZahlenderBetrag = fahrkartenbestellungErfassung();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wuenschen Ihnen eine gute Fahrt.\n");

		}
	}

	public static double fahrkartenbestellungErfassung() {
		int ticketauswahl = 0;
		double anzahleingabe = 0;
		double gesamtPreis = 0;
		Scanner tastatur = new Scanner(System.in);
		boolean bezahlen = false;
		
		// Preiseingabe
		
		
		while (bezahlen == false) {
			ticketauswahl=0;
			
			System.out.println("Waehlen Sie ihre Wunschfahrkarte fuer Berlin AB aus:");
			System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2");
			System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
			System.out.println("Bezahlen (9)");
			double preiseingabe = 0;
			while (!(ticketauswahl == 1 || ticketauswahl == 2 || ticketauswahl == 3 || ticketauswahl == 9)) {
				ticketauswahl = tastatur.nextInt();
				if (ticketauswahl == 1) {
					System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
					preiseingabe = 2.90;
				} else if (ticketauswahl == 2) {
					System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
					preiseingabe = 8.60;
				} else if (ticketauswahl == 3) {
					System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
					preiseingabe = 23.50;
				} else if (ticketauswahl == 9) {
					bezahlen = true;
				
				}
			}

			// Anzahleingabe und // Preisbrechnung
			if (bezahlen == false){
			System.out.println("Bitte geben Sie eine Anzahl an Tickets ein: ");
			anzahleingabe = tastatur.nextDouble();

			while (anzahleingabe < 1 || anzahleingabe > 10) {
				System.out.println(">> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
				anzahleingabe = tastatur.nextDouble();
			}

			gesamtPreis = (preiseingabe * anzahleingabe) + gesamtPreis;
		}
		}
		return gesamtPreis;
		
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("\n%.2f%s\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
			System.out.println("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			Scanner tastatur = new Scanner(System.in);
			double eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);

		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rueckgabebetrag > 0.0) {
			System.out.printf("%s%.2f%s\n", "Der R�ckgabebetrag in Hoehe von ", rueckgabebetrag, " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "Cent");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "Cent");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "Cent");
				rueckgabebetrag -= 0.05;
			}

		}
	}

	public static void warte(int millisekunde) {
		try {

			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + einheit);
	}
}
