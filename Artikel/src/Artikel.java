public class Artikel {
 private String name;
 private int artikelnummer;
 private double einkaufpreis;
 private double verkaufspreis;
 private int sollBestand;
 private int istBestand;
 
 public Artikel() {
	 
 }
 
public Artikel(String name, int artikelnummer, double einkaufpreis, double verkaufspreis, int sollBestand,
		int istBestand) {
	super();
	this.name = name;
	this.artikelnummer = artikelnummer;
	this.einkaufpreis = einkaufpreis;
	this.verkaufspreis = verkaufspreis;
	this.sollBestand = sollBestand;
	this.istBestand = istBestand;
}

@Override
public String toString() {
	return "Artikel [name=" + name + ", artikelnummer=" + artikelnummer + ", einkaufpreis=" + einkaufpreis
			+ ", verkaufspreis=" + verkaufspreis + ", sollBestand=" + sollBestand + ", istBestand=" + istBestand + "]";
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getArtikelnummer() {
	return artikelnummer;
}

public void setArtikelnummer(int artikelnummer) {
	this.artikelnummer = artikelnummer;
}

public double getEinkaufpreis() {
	return einkaufpreis;
}

public void setEinkaufpreis(double einkaufpreis) {
	this.einkaufpreis = einkaufpreis;
}

public double getVerkaufspreis() {
	return verkaufspreis;
}

public void setVerkaufspreis(double verkaufspreis) {
	this.verkaufspreis = verkaufspreis;
}

public int getSollBestand() {
	return sollBestand;
}

public void setSollBestand(int sollBestand) {
	this.sollBestand = sollBestand;
}

public int getIstBestand() {
	return istBestand;
}

public void setIstBestand(int istBestand) {
	this.istBestand = istBestand;
}
public void bestellen() {

	System.out.println("Ware wird nachbestellt");
		
}
public void lagerstandveraendern(int menge) {
	
}
public void berechneMarche() {
	
}
}
