//Aufgabe 2: Steuersatz

import java.util.Scanner;

public class steuersatz {
	
//Eingabe
	public static void main(String[] args) { 
		
		System.out.println("Bitte geben sie einen Nettobetrag ein: ");
		
		Scanner tastatur = new Scanner(System.in);
		double nettoWert = tastatur.nextDouble();
		double ermaeßigteSteuer = 0.07;
		double volleSteuer = 0.19;
		double bruttovoll = 0;
		double bruttoermaeßigt = 0;
		System.out.println("Für die Vollbesteuerung von 19% drücken Sie die Taste j = ja mit n = nein wird mit 7% besteuert !");
		char volleSteuerberechnung = tastatur.next().charAt(0);
		
//Verarbeitung
		
		
		if (volleSteuerberechnung == 'j') {
			bruttovoll = (nettoWert * volleSteuer) + nettoWert;
			System.out.printf("%s%.2f%s","Bruttobetrag inkl 19%: ", bruttovoll, " Euro");
		}
		else {
			bruttoermaeßigt = (nettoWert * ermaeßigteSteuer) + nettoWert;
			System.out.printf("%s%.2f%s", "Bruttobetrag inkl 7%: " , bruttoermaeßigt, " Euro");
		}
		
		
	}
}