import java.util.Scanner;


public class Volumenberechnung {
	 public static void main(String[] args) { 
		 
		 // Eingabe
		
		 // Anweisung
		 double seiteWürfel = eingabe("Bitte geben Sie einen Wert für die Kantenlänge für einen Würfel ein: ");
		
		 double hoeheQuader = eingabe("Bitte geben Sie 1 Wert für die Höhe des Quaders ein: ");
		
		 double laengeQuader = eingabe(" Bitte geben Sie 1 Wert für die Länge des Quaders ein: ");
		 
		 double breiteQuader = eingabe("Bitte geben Sie 1 Wert für die Länge des Queaders ein: ");
		
		 double breitepyramide = eingabe("Bitte geben Sie 1 Wert für die Breite einer Pyramide ein: ");
		 
		 double hoehepyramide = eingabe ("Bitte geben Sie 1 Wert für die Höhe einer Pyramide ein: ");
		 
		 double radiusKugel = eingabe ("Bitte geben sie 1 Wert für den Radius einer Kugel ein: ");
		 
		 //double seiteWürfel = 5;
		 
		 //double hoeheQuader = 5;
		 //double laengeQuader = 10;
		 //double breiteQuader = 4;
		 
		 //double breitePyramide = 5;
		 //double hoehePyramide = 5;
		 
		 //double radiusKugel = 2;
		 
		 //Verarbeitung
		 double volumenWuerfel = berechnungWuerfel (seiteWürfel);
		 double volumenQuader = berechnungQuader(hoeheQuader, laengeQuader, breiteQuader);
		 double volumenPyramide = berechnungPyramide (breitepyramide, hoehepyramide);
		 double volumenKugel = berechnungKugel (radiusKugel);
		
		 //System.out.printf("%s%.2f", "Volumen des Würfels: ", volumenWuerfel);
		 //System.out.printf("\n%s%.2f", "Volumen des Quaders: ", volumenQuader);
		 //System.out.printf("\n%s%.2f", "Volumen der Pyramide: ", volumenPyramide);
		 //System.out.printf("\n%s%.2f", "Volumen der Kugel: ", volumenKugel);
	 	
		 
		 //Ausgabe
	 	ausgabe ("Würfel", volumenWuerfel);
	 	ausgabe ("Quader", volumenQuader);
	 	ausgabe ("Pyramide", volumenPyramide);
	 	ausgabe ("Kugel", volumenKugel);
	 	
	 }
	 	public static double eingabe(String anweisung) {
	 		System.out.print(anweisung);
	 		Scanner tastatur = new Scanner(System.in);
	 		double eingabe = tastatur.nextDouble();
	 		return eingabe;
	 		
	 	}
	 
	 	
		 public static void titel() {
				System.out.println("Dieses Programm berechnet die Volumen verschiedener Formen");
				System.out.println("---------------------------------------------");
			}		
		 
		
		public static double berechnungWuerfel (double a) {
			return (a * a * a) ;
		}
		public static double berechnungQuader (double a, double b, double c) {
			return (a * b * c);
		}
		public static double berechnungPyramide (double a, double h) {
			return (a * a * h /3);
		}
		public static double berechnungKugel (double r) {
			double pi = 3.14;
			return (4/3 * r * r * r * pi);			
		}
		
		public static void ausgabe(String form, double volumen ) {
			System.out.printf("\n%s%s%.2f", form , ": " , volumen );
		}
			
}
