import java.util.Scanner;

import javax.print.attribute.standard.RequestingUserName;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.TitlePaneLayout;

import org.omg.PortableServer.ServantActivator;

import com.sun.javafx.binding.StringFormatter;

public class Volumenberechnung_neu {
	public static void main (String[] args) {
	
	double seiteWuerfel;

	double laengeQuader;
	double breiteQuader;
	double hoeheQuader;
	
	double breitePyramide;
	double hoehePyramide;
	
	double radiusKugel;
	
	double volumenWuerfel; 
	double volumenQuader;
	double volumenPyramide;
	double volumenKugel;
	
	
	

	//Eingabe:
	//W�rfel
	//seiteWuerfel = tastatur.nextDouble();
	//Quader
	//System.out.print("\nBitte geben die einen Wert f�r die L�nge des Quaders ein: ");
	//laengeQuader = tastatur.nextDouble();
	//System.out.print("\nBitte geben die einen Wert f�r die Breite des Quaders ein: ");
	//breiteQuader = tastatur.nextDouble();
	//System.out.print("\nBitte geben die einen Wert f�r die H�he des Quaders ein: ");
	//hoeheQuader = tastatur.nextDouble();
	//Pyramide
	//System.out.print("Bitte geben die einen Wert f�r die Breite der Pyramide ein: ");
	//breitePyramide = tastatur.nextDouble();
	//System.out.print("Bitte geben die einen Wert f�r die H�he der Pyramide ein: ");
	//hoehePyramide = tastatur.nextDouble();
	//Kugel
	//System.out.print("Bitte geben die einen Wert f�r den Radius einer Kugel ein: ");
	//radiusKugel = tastatur.nextDouble();
	
	seiteWuerfel = eingabe("Bitte geben Sie einen Wert f�r die Seite eines W�rfels an: ");
	
	laengeQuader = eingabe("\nBitte  geben Sie einen Wert f�r die L�nge des Quaders ein: ");
	breiteQuader = eingabe ("\nBitte geben Sie einen Wert f�r die L�nge des Quaders ein: ");
	hoeheQuader = eingabe("\nBitte geben die einen Wert f�r die H�he des Quaders ein: ");
	
	breitePyramide = eingabe("Bitte geben die einen Wert f�r die Breite der Pyramide ein: ");
	hoehePyramide = eingabe("Bitte geben die einen Wert f�r die Breite der Pyramide ein: ");
	
	//Verarbeitung:
	volumenWuerfel = berechnungWuerfel(seiteWuerfel);
	volumenQuader = berechnungQuader(laengeQuader, breiteQuader, hoeheQuader);
	volumenPyramide = berechnungPyramide(breitePyramide, hoehePyramide);
	//volumenKugel = berechnungKugel(radiusKugel);
	
	hinweis("\nHier werden Volumen unterschiedlicher Formen berechnet!");
	
	//Ausgabe:
	ausgabe("W�rfel: " , volumenWuerfel);
	ausgabe("Quader: " , volumenQuader);
	ausgabe("Pyramide: ", volumenPyramide);
	//ausgabe("Kugel: ", volumenKugel);
	//System.out.println(volumenWuerfel);
	//System.out.println(volumenQuader);
	//System.out.println(volumenPyramide);
	//System.out.println(volumenKugel);
	}
	
	public static double eingabe(String Anfrage) {
		System.out.print(Anfrage);
		Scanner tastatur = new Scanner(System.in);
		double wert = tastatur.nextDouble();
		return wert;
	}

	public static double berechnungWuerfel(double a){
		double ergebnis = a*a*a;
		return ergebnis;
	}
	public static double berechnungQuader(double a, double b, double c){
		double ergebnis = a*b*c;
		return ergebnis;
	}
	public static double berechnungPyramide(double a, double h){
		double ergebnis = a*a*h /3;
		return ergebnis;
	}
	public static double berechnungKugel(double r) {
		double pi = 3.14;
		double ergebnis = 4/3 * r * r * r * pi;
		return ergebnis;
	}	
	public static void hinweis(String Aussage) {
		System.out.print(Aussage);
	}
	
	public static void ausgabe(String form, double volumen ) {
		System.out.printf("\n%s%s%.2f", form , ": " , volumen );
	}
	
}
