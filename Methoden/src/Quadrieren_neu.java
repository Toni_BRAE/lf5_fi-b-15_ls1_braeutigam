import java.util.Scanner;

public class Quadrieren_neu {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f¸r x festlegen:
		// ===========================
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine Zahl ein: ");
		
		double x = myScanner.nextDouble();
		System.out.println("Dieses Programm berechnet die Quadratzahl x≤");
		System.out.println("---------------------------------------------");
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis= x * x;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x≤= %.2f\n", x, ergebnis);
	}	
}
