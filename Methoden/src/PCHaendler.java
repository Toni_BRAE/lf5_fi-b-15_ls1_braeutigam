import java.util.Scanner;

public class PCHaendler {
	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

		String artikel;
		int anzahl;
		double nettogesamtpreis;
		double preis;
		double bruttogesamtpreis;

		// Benutzereingaben lesen
		artikel = liesString("was m�chten Sie bestellen?");

		anzahl = liesInt("Geben Sie die Anzahl ein:");

		preis = liesDouble("Geben Sie den Nettopreis ein:");

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis,mwst);

	}

	public static String liesString(String text) {
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}

	public static int liesInt(String text) {
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}

	public static double liesDouble(String text) {
		System.out.println(text);
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double 
			nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, 
			double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	public static void rechnungausgeben(String artikel, int anzahl, double 
			nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}