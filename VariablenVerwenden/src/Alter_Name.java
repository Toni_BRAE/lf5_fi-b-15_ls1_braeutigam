import java.util.Scanner;

public class Alter_Name  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Nennen Sie Ihren Namen: ");     
    String s = myScanner.next();
    
    System.out.print("Nennen Sie ihr Alter: ");     
    int alter = myScanner.nextInt();  
    
    System.out.print("Ihr Name ist: " + s + " Ihr Alter: " + alter);      
  }
}