import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnis = zahl1 + zahl2;  
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
 
    ergebnis = zahl1 - zahl2;  
    
    System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis); 
    
    ergebnis = zahl1 * zahl2;  
    
    System.out.print("\n\n\nErgebnis der Multiplication lautet: "); 
    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis); 
    
    ergebnis = zahl1 / zahl2;  
    
    System.out.print("\n\n\nErgebnis der Diffision lautet: "); 
    System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis); 
    
    
    myScanner.close(); 
    
  }
}

//double z1 = 1.0 / 2.0
//double z2 =(double)1 /(double)2